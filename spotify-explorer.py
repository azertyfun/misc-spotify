#!/usr/bin/env python3

'Creates similar playlists while removing duplicates'

# pylint: disable = invalid-name, line-too-long

import sys

from spotutil import connect, get_tracks, get_user_tracks, normalize, format_track

def main():
    'Main function'

    try:
        username = sys.argv[1]
        playlist_id = sys.argv[2]

        allow_covers = True
        if len(sys.argv) >= 4:
            allow_covers = sys.argv[3]
            if allow_covers.lower() in ('1', 'true', 'yes'):
                allow_covers = True
            elif allow_covers.lower() in ('0', 'false', 'no'):
                allow_covers = False
            else:
                print('Can\'t parse \'%s\' for allow_covers' % allow_covers)
                sys.exit(1)
    except IndexError:
        print('Usage: %s <username> <playlist ID> [allow_covers=yes]' % sys.argv[0], file=sys.stderr)
        sys.exit(1)

    sp = connect(username)
    current_user = sp.current_user()

    saved_tracks = get_user_tracks(sp)
    saved_tracks_normalized = normalize(saved_tracks, allow_covers)
    tracks = get_tracks(sp, sp.playlist(playlist_id, fields='tracks,next')['tracks'])
    tracks_normalized = normalize(tracks, allow_covers)

    saved_tracks_normalized_set = set(saved_tracks_normalized.values())
    to_remove = [
        track_id
        for track_id in tracks_normalized
        if tracks_normalized[track_id] in saved_tracks_normalized_set
    ]

    print('Will remove the following tracks:')
    print('\n'.join([format_track(tracks[e]) for e in to_remove]))

    sure = None
    while sure not in ('y', 'n'):
        sure = input('Are you sure (y/n)? ')
    if sure == 'n':
        print('OK, aborting.')
        sys.exit(0)

    for i in range(0, len(to_remove), 100):
        print(sp.user_playlist_remove_all_occurrences_of_tracks(current_user['id'], playlist_id, to_remove[i:i + 100]))

if __name__ == '__main__':
    main()
