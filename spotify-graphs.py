#!/usr/bin/env python3

'Creates similar playlists while removing duplicates'

# pylint: disable = invalid-name, line-too-long

import csv
import sys

from spotutil import connect, get_tracks, get_user_tracks, format_track

def main():
    'Main function'

    try:
        username = sys.argv[1]
    except IndexError:
        print('Usage: %s <username>' % sys.argv[0], file=sys.stderr)
        sys.exit(1)

    sp = connect(username)
    # current_user = sp.current_user()
    tracks = list(get_user_tracks(sp).values())

    features = {}
    for i in range(0, len(tracks), 50):
        f = sp.audio_features([t['track']['id'] for t in tracks[i:i+50]])
        for e in f:
            features[e['id']] = e

    writer = csv.writer(sys.stdout, delimiter='\t')
    writer.writerow([
        'ID', 'Title', 'Artists', 'Album', 'Release Date', 'Added', 'Popularity', 'Duration (ms)',
        'Danceability', 'Energy', 'Key', 'Loudness', 'Mode', 'Speechiness', 'Acousticness', 'Instrumentalness', 'Liveness', 'Valence', 'Tempo', 'Time Signature'
    ])
    for track in tracks:
        i = track['track']['id']
        writer.writerow([
            i,
            track['track']['name'],
            ', '.join([a['name'] for a in track['track']['artists']]),
            track['track']['album']['name'],
            '{}{}'.format(track['track']['album']['release_date'], '-01-01' if track['track']['album']['release_date_precision'] == 'year' else ''),
            track['added_at'],
            track['track']['popularity'],
            track['track']['duration_ms'],

            features[i]['danceability'],
            features[i]['energy'],
            features[i]['key'],
            features[i]['loudness'],
            features[i]['mode'],
            features[i]['speechiness'],
            features[i]['acousticness'],
            features[i]['instrumentalness'],
            features[i]['liveness'],
            features[i]['valence'],
            features[i]['tempo'],
            features[i]['time_signature'],
        ])

if __name__ == '__main__':
    main()
