'Defines some function to be used by the scripts in this folder'

# pylint: disable = invalid-name, line-too-long

import json
import os
import sys
import time

import unicodedata
from typing import Dict, Tuple, Union
from pathlib import Path

import spotipy
import spotipy.util

if os.getenv('XDG_CACHE_HOME'):
    CACHE_DIR = Path(os.getenv('XDG_CACHE_HOME'))
else:
    CACHE_DIR = Path(os.getenv('HOME')) / '.cache'
CACHE_DIR.mkdir(exist_ok=True)

TRACKS_FILE = CACHE_DIR / 'spotify-tracks.json'
TRACKS_FILE_VALIDITY = 3600

class NotCached(RuntimeWarning):
    'Not found in cache. Please fetch at source.'

def get_tracks(sp: spotipy.Spotify, tracks: dict) -> Dict[str, dict]:
    'Gets all the tracks from a paginated spotify track list, mapped by track ID'

    out = {}
    def extend(t):
        out.update({
            i['track']['id']: i
            for i in t['items']
        })

    extend(tracks)
    while tracks['next']:
        tracks = sp.next(tracks)
        extend(tracks)

    return out

def get_user_tracks(sp: spotipy.Spotify) -> Dict[str, dict]:
    'Gets all the tracks for the current user, using a cache'

    try:
        if not TRACKS_FILE.exists():
            raise NotCached()
        with TRACKS_FILE.open() as fp:
            j = json.load(fp)
            if time.time() - j['time'] > TRACKS_FILE_VALIDITY:
                raise NotCached()
            return j['tracks']
    except NotCached:
        tracks = get_tracks(sp, sp.current_user_saved_tracks())
        with TRACKS_FILE.open('w') as fp:
            json.dump({
                'time': time.time(),
                'tracks': tracks,
            }, fp)
        return tracks

def normalize(tracks: Dict[str, dict], allow_covers: bool) -> Dict[str, Tuple[Union[None, str], str]]:
    'Transforms a spotify track list into a dictionary, mapping track ID to a normalized title for comparison with other track titles'

    return {
        track['track']['id']: (
            ','.join([
                artist['id']
                for artist in track['track']['artists']
                if artist['id']
            ]) if not allow_covers else None,
            unicodedata.normalize('NFKD', track['track']['name']).split(' - ')[0].split('(')[0].lower().strip()
        )
        for track in tracks.values()
    }

def format_track(track: dict) -> str:
    'Returns a user-readable name for the track, with artists'

    return '%s – %s' % (', '.join([a['name'] for a in track['track']['artists']]), track['track']['name'])


def connect(username: str) -> spotipy.Spotify:
    'Prompt for oAuth and create Spotify object'

    scope = 'playlist-modify-private,user-library-read'
    token = spotipy.util.prompt_for_user_token(username, scope)
    if not token:
        print('Could not get token for %s' % username, file=sys.stderr)
        sys.exit(1)

    return spotipy.Spotify(auth=token)
